# CONQUER THE WORLD
# TEAM-97
# Team Members <br>
U.Tejasree-20WH1A05A2-CSE <br>K.Kavitha-20WH1A1276-IT<br>D.Bhavani-20WH1A0456-ECE<br>CH.Prathyusha-20WH1A05G3-CSE<br>S.Ankitha-21WH5A0510-CSE<br>Bhavishya-20WH1A05C9-CSE
# PROBLEM STATEMENT
CONQUER THE WORLD . Now a days based on the theme we are going to play the game.Armies of the nation are sent to other countries to conquer the world.Now we know the nations mapand we have to send the armies to different nations to subjugate it.The problem statement says that using minimum cost we need to have a maximum flow of armies into the nations.
# APPROACH
We have a map of the nations of the world and all are available transport routes between them they are u,v.Each room connects through nations and has a fixes cost per army that uses it that is C.The routes are laid out such that there is exactly one way to travel between any two nations .We know the current position of each of our armies Xi and how many we will need to place permanently in each nation Yi in order to subjugate.Now using the minimum cost maximum flow or Dijksta's shortest path algorithm we approach the problem statement.
# REFERENCES <br>
https://www.topper.com<br>https://www.geeksforgeeks.org
# CHALLENGES
intially understanding the problem statement was difficult.

